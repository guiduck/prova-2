package model;

public class Medico extends Usuario {

	public Medico(String nome) {
		super();
		// TODO Auto-generated constructor stub
	}

	private String especializacao, convenio;

	public String getEspecializacao() {
		return especializacao;
	}

	public void setEspecializacao(String especializacao) {
		this.especializacao = especializacao;
	}

	public String getConvenio() {
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}
	
	

}
